#version 140

in vec3 position;           // vertex position in world space
in vec3 normal;             // vertex normal
in vec2 texCoord;           // incoming texture coordinates

uniform mat4 PVMmatrix;     // Projection * View * Model  --> model to clip coordinates
uniform mat4 Vmatrix;       // View                       --> world to eye coordinates
uniform mat4 Mmatrix;       // Model                      --> model to world coordinates
uniform mat4 normalMatrix;  // inverse transposed Mmatrix

smooth out vec2 texCoord_v;  // outgoing texture coordinates
smooth out vec3 position_v;
smooth out vec3 normal_v;

void main() {
	// vertex position after the projection (gl_Position is built-in output variable)
	gl_Position = PVMmatrix * vec4(position, 1);   // out:v vertex in clip coordinates

	texCoord_v = texCoord;

	// eye-coordinates position and normal of vertex
	position_v = (Vmatrix * Mmatrix * vec4(position, 1.0)).xyz;               // vertex in eye coordinates
	normal_v = normalize((Vmatrix * normalMatrix * vec4(normal, 0.0)).xyz);   // normal in eye coordinates by NormalMatrix
}
