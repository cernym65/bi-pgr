#version 140

struct Material {                 // structure that describes currently used material
	vec3  ambient;                // ambient component
	vec3  diffuse;                // diffuse component
	vec3  specular;               // specular component
	float shininess;              // sharpness of specular reflection

	bool  useTexture;             // defines whether the texture is used or not
};

struct Light {                    // structure describing light parameters
	vec3  ambient;                // intensity & color of the ambient component
	vec3  diffuse;                // intensity & color of the diffuse component
	vec3  specular;               // intensity & color of the specular component
	vec3  position;               // light position
	vec3  spotDirection;          // spotlight direction

	float spotCosCutOff;          // cosine of the spotlight's half angle
	float spotExponent;           // distribution of the light energy within the reflector's cone (center->cone's edge)
};

uniform sampler2D texSampler;     // sampler for the texture access

uniform Material material;        // current material
uniform float time;               // time used for simulation of moving lights (such as sun)
uniform vec3 reflectorPosition;   // reflector position (world coordinates)
uniform vec3 reflectorDirection;  // reflector direction (world coordinates)
uniform mat4 Vmatrix;             // View                       --> world to eye coordinates

smooth in vec2 texCoord_v;        // fragment texture coordinates
smooth in vec3 position_v;
smooth in vec3 normal_v;          // vertex normal

out vec4       color_f;           // outgoing fragment color

vec4 spotLight(Light light, Material material, vec3 vertexPosition, vec3 vertexNormal) {
	vec3 ret = vec3(0.0);

	// use the material and light structures to obtain the surface and light properties
	// the vertexPosition and vertexNormal variables contain transformed surface position and normal
	// store the ambient, diffuse and specular terms to the ret variable
	// for spot lights, light.position contains the light position
	// everything is expressed in the view coordinate system -> eye/camera is in the origin

	// ======== BEGIN OF SOLUTION - TASK 3_3-2 ======== //
		 
	vec3 L = normalize(light.position - vertexPosition);
	vec3 R = reflect(-L, vertexNormal);
	vec3 V = normalize(vec3(0.0) - vertexPosition);

	vec3 ambient = light.ambient * material.ambient;

	float spotlightEffect = dot(light.spotDirection, -L);
	if (spotlightEffect < light.spotCosCutOff)
		return vec4(ambient, 1.0);

	spotlightEffect = pow(max(spotlightEffect, 0.0), light.spotExponent);

	vec3 diffuse = max(dot(L, vertexNormal), 0.0) * light.diffuse * material.diffuse;
	vec3 specular = pow(max(dot(R, V), 0.0), material.shininess) * light.specular * material.specular;

	ret = ambient + (diffuse + specular) * spotlightEffect;

	// ========  END OF SOLUTION - TASK 3_3-2  ======== //

	return vec4(ret, 1.0);
}

vec4 directionalLight(Light light, Material material, vec3 vertexPosition, vec3 vertexNormal) {
	vec3 ret = vec3(0.0);

	// use the material and light structures to obtain the surface and light properties
	// the vertexPosition and vertexNormal variables contain transformed surface position and normal
	// store the ambient, diffuse and specular terms to the ret variable
	// glsl provides some built-in functions, for example: reflect, normalize, pow, dot
	// for directional lights, light.position contains the direction
	// everything is expressed in the view coordinate system -> eye/camera is in the origin

	// ======== BEGIN OF SOLUTION - TASK 3_2-1 ======== //

	vec3 L = normalize(light.position - vertexPosition);
	vec3 R = reflect(-L, vertexNormal);
	vec3 V = normalize(vec3(0.0) - vertexPosition);

	vec3 ambient = light.ambient * material.ambient;
	vec3 diffuse = max(dot(L, vertexNormal), 0.0) * light.diffuse * material.diffuse;
	vec3 specular = pow(max(dot(R, V), 0.0), material.shininess) * light.specular * material.specular;

	ret = ambient + diffuse + specular;

	// ========  END OF SOLUTION - TASK 3_2-1  ======== //

	return vec4(ret, 1.0);
}

// hardcoded lights
Light sun;
float sunSpeed = 0.5f;
Light spaceShipReflector;

void setupLights() {

	// set up sun parameters
	sun.ambient = vec3(0.0);
	sun.diffuse = vec3(1.0, 1.0, 0.5f);
	sun.specular = vec3(1.0);

	// ======== BEGIN OF SOLUTION - TASK 3_2-2 ======== //

	// calculate correct direction to the sun using the time and sunSpeed variables
	// sun has to rotate in XZ plane in world coordinates
	// dont forget to translate the direction to the view coordinates (using Vmatrix)
	sun.position = (Vmatrix * vec4(cos(time), 1.0, sin(time), 1.0)).xyz;

	// ========  END OF SOLUTION - TASK 3_2-2  ======== //

	// set up reflector parameters
	spaceShipReflector.ambient = vec3(0.2f);
	spaceShipReflector.diffuse = vec3(1.0);
	spaceShipReflector.specular = vec3(1.0);
	spaceShipReflector.spotCosCutOff = 0.95f;
	spaceShipReflector.spotExponent = 1.0;

	// ======== BEGIN OF SOLUTION - TASK 3_3-3 ======== //

	  // set properly reflector direction and position taking into account reflectorPosition
	  // and reflectorDirection uniforms

	  spaceShipReflector.position = (Vmatrix * vec4(reflectorPosition, 1.0)).xyz;
	  spaceShipReflector.spotDirection = normalize((Vmatrix * vec4(reflectorDirection, 0.0)).xyz);

	// ========  END OF SOLUTION - TASK 3_3-3  ======== //
}


void main() {
	vec3 normalN = normalize(normal_v);

	setupLights();

	// initialize the output color with the global ambient term
	vec3 globalAmbientLight = vec3(0.4f);
	vec4 outputColor = vec4(material.ambient * globalAmbientLight, 0.0);

	// accumulate contributions from all lights
	outputColor += directionalLight(sun, material, position_v, normalN);
	outputColor += spotLight(spaceShipReflector, material, position_v, normalN);

	color_f = outputColor;

	// if material has a texture -> apply it
	if (material.useTexture)
		color_f = outputColor * texture(texSampler, texCoord_v);
}
